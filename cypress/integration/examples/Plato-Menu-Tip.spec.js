/// <reference types="cypress" />

describe("¿Todo ok de acuerdo al diseño?", () => {
    beforeEach(() => {
        cy.visit("https://staging.admin.aplatopuesto.jaque.dev/login");
    });


    it("Validando casos estaticos en Login Plato Puesto", () => {

        cy.get('#login > div.login-container > img').should('be.visible')

        cy.get('#login > div.login-container > p.h1').should('contain.text', 'Inicia sesión');

        cy.get('#login > div.login-container > p:nth-child(3)').should('contain.text', "\n      Inicia sesión y comienza a administrar tu negocio conA Plato Puesto\n    ")


        cy.get('#mat-form-field-label-1 > mat-label').should('contain', 'Correo electrónico');
        cy.get('#mat-form-field-label-3 > mat-label').should('contain', 'Contraseña');

        cy.get('#login > div.login-container > form > div > a').should('be.visible').and('contain.text', 'Olvidé mi contraseña')

        cy.get('#login > div.login-container > form > div > button').should('be.visible').and('contain.text', 'Iniciar sesión')

    })

})