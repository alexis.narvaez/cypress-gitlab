/*

/// <reference types="cypress" />

describe("Primer conjunto de casos de prueba", () => {
  beforeEach(() => {
    cy.visit("http://automationpractice.com/index.php");
  });

  // Caso de prueba
  // it("Contabilizar la cantidad de elementos en la sección de página principal", () => {
  //   // Verificar el tamaño de elementos
  //   cy.get("#homefeatured .product-container").should("have.length", 7);

  //   // Nombrando la lista para poder ser utilizada despues.
  //   cy.get("#homefeatured .product-container").as("productosPopulares");

  //   cy.get("@productosPopulares").should("have.length", 7);
  // });

  // it('Agregar elementos de tipo "Blouse" al carrito de compra desde la página principal', () => {
  //     cy.get('#homefeatured .product-container').as('productosPopulares')
  //     cy.get('@productosPopulares')
  //     .find('.product-name')
  //     .each(($el, idx, $list) => {
  //         if ($el.attr('title') === 'Blouse') {
  //             cy.log('Se ha encontrado el elemento buscado')
  //             // cy.get('@productosPopulares').eq(idx).find('.button.ajax_add_to_cart_button span').click();
  //             cy.get('@productosPopulares').eq(idx).contains('Add to cart').click();
  //         }

  //     })

  // });

  // it('Agregar elementos de tipo "Printed Dress" al carrito de compra desde la página principal', () => {
  //   cy.get("#homefeatured .product-container").as("productosPopulares");
  //   cy.get("@productosPopulares")
  //     .find(".product-name")
  //     .each(($el, idx, $list) => {
  //       cy.get("@productosPopulares")
  //         .eq(idx)
  //         .find(".price")
  //         .then(($el1) => {
  //           let price = $el1.text();
  //           if (
  //             $el.attr("title") === "Printed Dress" &&
  //             price.includes("50.99")
  //           ) {
  //             cy.log("Se ha encontrado el elemento buscado");
  //             cy.log("Se ha encontrado el precio buscado");
  //             // cy.get('@productosPopulares').eq(idx).find('.button.ajax_add_to_cart_button span').click();
  //             cy.get("@productosPopulares")
  //               .eq(idx)
  //               .contains("Add to cart")
  //               .click();
  //           }
  //         });
  //     });

  //     cy.get('h2 > .ajax_cart_product_txt').should('contain.text', 'There is 1 item in your cart.').should('be.visible')
  // });


  // it('Verificamos que el drop down de women, tenga los elementos necesarios', () => {

  //   cy.get('#block_top_menu > ul > li:nth-child(1) > ul').invoke('attr','style', 'display: block');
  //   cy.get('a[title="Tops"]').should('be.visible')
  //   cy.get('a[title="T-shirts"]').should('be.visible')
  //   cy.get('a[title="Blouses"]').should('be.visible')
  //   cy.get('a[title="Dresses"]').should('be.visible')
  //   cy.get('a[title^="Casual"]').should('be.visible')
  //   cy.get('a[title^="Evening"]').should('be.visible')
  //   cy.get('a[title^="Summer"]').should('be.visible')
  //   // cy.get('ul .submenu-container > li:nth-child(1) > ul').invoke('attr','style', 'display: block');

  // });

  it('Haciendo una compra desde 0', () => {

    cy.get('#search_query_top').type('Blouse');
    cy.get('#searchbox > .btn').click();

    cy.wait(1000);

    cy.scrollCenterLinear('#center_column > ul', 2000)
    // cy.get('#center_column > ul').scrollIntoView({ duration: 2000, easing: 'linear' });
    cy.get('#center_column > ul > li > div > div.right-block > h5 > a').should('contain.text', 'Blouse')

    cy.wait(500);

    cy.get('.product_list .ajax_block_product .ajax_add_to_cart_button > span').click();

    cy.scrollCenterLinear('#layer_cart .clearfix', 500)
    
    cy.get('#layer_cart .clearfix .button-container .button-medium').scrollIntoView({ duration: 500, easing: 'linear' }).click('center', { force: true });

    cy.scrollCenterLinear('#center_column', 600)

    cy.get('.cart_description > .product-name > a').should('contain.text', 'Blouse')

    cy.get('.cart_unit > .price > span').should('contain.text', '$27.00')

    cy.wait(500);

    cy.get('.cart_navigation > .button > span').click('center', { force: true });

    cy.wait(500);

    cy.scrollCenterLinear('#center_column', 500)

    cy.get('#email').type('alexisqa@gmail.com', { scrollBehavior: 'center', delay: 20}).should('have.value', 'alexisqa@gmail.com')
    cy.wait(200);
    cy.get('#passwd').type('pozol123', { scrollBehavior: 'center', delay: 20});
    cy.wait(200);
    cy.get('#SubmitLogin > span').click('center', { force: true })

    cy.scrollCenterLinear('#ordermsg > .form-control', 3000)

    /* Button continue */
//     cy.get('.cart_navigation > .button > span').as('buttonContinueSteps')

//     cy.get('@buttonContinueSteps').click('center', { force: true })

//     cy.scrollCenterLinear('#carrier_area', 500)
//     cy.wait(1000);

//     cy.get('#cgv').check({ scrollBehavior: 'center'});
//     cy.get('@buttonContinueSteps').click('center', { force: true })

//     cy.wait(500);

//     cy.scrollCenterLinear('#order-detail-content', 500)
//     cy.get('.cart_description > .product-name').should('contain.text', 'Blouse')
//     cy.wait(500)

//     cy.get('.bankwire').click('center', { force: true })

//     cy.scrollCenterLinear('.box', 500)
//     cy.get('@buttonContinueSteps').click('center', { force: true })

//     cy.scrollCenterLinear('#center_column', 500)
//     cy.get('.cheque-indent > .dark').should('contain.text', 'Your order on My Store is complete.')

//   });

// });

